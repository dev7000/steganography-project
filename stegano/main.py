from scipy.fftpack import *
from PIL.Image import *
from hashlib import *
from numpy import *
from zlib import *

QM = array([[16,11,10,16,24,40,51,61],[12,12,14,19,26,58,60,55],[14,13,16,24,40,57,69,56 ],[14,17,22,29,51,87,80,62],[18,22,37,56,68,109,103,77],[24,35,55,64,81,104,113,92],[49,64,78,87,103,121,120,101],[72,92,95,98,112,100,103,99]])
QM = QM / 10


##############################################################################################################################################

def zigzag(block):
	res = []
	for i in range(7):
		for j in range(i+1):
			res.append(block[j,i-j])

	for d in range(8):
		res.append(block[d,7-d])

	for i in range(7):
		for j in range(7-i):
			res.append(block[1+i+j,7-j])
	
	while len(res)>0 and res[-1] == 0:
		res.pop()

	return res


def block8x8(zigzag): 
	res = zeros(64).reshape(8,8)
	k = 0

	for i in range(7):
		for j in range(i+1):
			if k == len(zigzag):
				return res
			res[j,i-j] = zigzag[k]
			k += 1

	for d in range(8):
		if k == len(zigzag):
			return res
		res[d,7-d] = zigzag[k]
		k += 1

	for i in range(7):
		for j in range(7-i):
			if k == len(zigzag):
				return res
			res[1+i+j,7-j] = zigzag[k]
			k += 1

	return res


def stegano(cov,img,lsb):

	stream = []

	imshape = img.shape

	rows, cols = int(ceil(img.shape[0]/8.0)), int(ceil(img.shape[1]/8.0))

	fimg = flip(img,axis=1)
	img = concatenate((img,fimg),axis=1)
	fimg = flip(img,axis=0)
	img = concatenate((img,fimg),axis=0)


	for row in range(rows):
		for col in range(cols):
			r ,c = row*8 , col*8
			block = img[r:r+8, c:c+8].astype(int)
			block -= 128

			imgdct = dct(block,norm='ortho',axis=0)
			imgdct = dct(imgdct,norm='ortho',axis=1)
			imgdct = (imgdct/QM).astype(int)

			stream += zigzag(imgdct) + [1024]

	shape = cov.shape
	cov = cov.flatten()
	
	msg=''
	for w in stream:
		if w < 0:
			w = -w + 1024
		msg += "{0:011b}".format(w)
	msg = "{0:012b}".format(imshape[0]) + "{0:012b}".format(imshape[1]) + "{0:032b}".format(adler32(msg)& 0xffffffff) +msg

	nbytes = int(ceil(len(msg)/float(lsb)))
	msg += '0'*lsb
	msg = msg[:nbytes*lsb]


	if nbytes > len(cov):
		return False

	for i in range(len(cov)):
		ii = i % nbytes
		bits = msg[ii*lsb:ii*lsb+lsb]
		bits = int(bits,2)
		cov[i] /= 2**lsb
		cov[i] *= 2**lsb	
		cov[i] += bits

	return cov.reshape(shape)


def retreive(img,lsb):
	img = img.flatten()

	msg = ''
	for byte in img:
		msg += '{0:08b}'.format(byte)[8-lsb:8]


	mshape = [int(msg[:12],2),int(msg[12:24],2)]
	mlen = int(ceil(mshape[0]/8.0))*int(ceil(mshape[1]/8.0))
	msg = msg[24:]
	mhash = int(msg[0:32],2)
	msg = msg[32:]


	recv = array([[0 for j in range(mshape[1]+8)] for i in range(mshape[0]+8)])

	zigzag = []
	blockno,i = 0,0
	while blockno<mlen:
		x = msg[i*11:i*11+11]
		i += 1

		x = int(x,2)
		if x == 1024:
			row = blockno / int(ceil(mshape[1]/8.0))
			col = blockno - row * int(ceil(mshape[1]/8.0))

			r ,c = row*8 , col*8
			imgdct = block8x8(zigzag)

			rimage = imgdct*QM
			rimage = idct(rimage,norm='ortho',axis=0)
			rimage = idct(rimage,norm='ortho',axis=1)

			recv[r:r+8, c:c+8] = rimage.astype(int)+128


			zigzag = []
			blockno += 1
			continue

		if x > 1024:
			x -= 1024
			x = -x
		zigzag.append(x)

	return recv[:mshape[0],:mshape[1]]



#################################################################################################################################



cov = open("cover.jpg")
cov = array(cov)

img = open("tower.jpg")
img = img.convert("L")
img = array(img)

lsb = 1

steg = stegano(cov,img,lsb)
if steg is False:
	print "Given input Image Size cannot to embedded in the cover Image"
	exit()

fromarray(steg.astype('uint8')).save('steganographic_image.png')


steg = open("steganographic_image.png")
steg = array(steg)


ret = retreive(steg,lsb)
fromarray(ret.astype('uint8')).save('retreived_secret_image.png')




