from PIL import Image
import numpy as np
import math
import random
import time
from random import sample
from statistics import mean


def euc_dis(a,b):
	# a = pv[x1]
	# b = pv[x2]
	d = (a[0]-b[0])**2 +(a[1]-b[1])**2 +(a[2]-b[2])**2 
	d1 = math.sqrt(d)
	return d1

def lsb(x,a):
	h = x
	if (h%2) == 0:
		h = h+a
	else:
		h = h-1+a

def totuple(a):
    try:
        return tuple(totuple(i) for i in a)
    except TypeError:
        return a

get_bin = lambda x, n: format(x, 'b').zfill(n)
def steg(parts,num1,k,pv):
	for j in range(k):
		x1 = 0
		y1=0
		st = parts[j]
		l1 = len(st)
		for j1 in range(l1):
			h = ord(st[j1])
			cc = get_bin(h,8)
			cc = str(cc)
			# print(type(cc))
			d=0
			for c in cc:
				d=d+1
				c = int(c)
				# print(j)
				# print(x1)
				# print(len(num1[4]))
				# print(num1[j][x1])
				h = pv[num1[j][x1]][y1]
				if (h%2) == 0:
					h = h+c
				else:
					h = h-1+c
				# pv[num1[j][x1]][y1] = lsb(pv[num1[j][x1]][y1],c)
				pv[num1[j][x1]][y1] = h


				if y1==2:
					y1=0
					x1=x1+1
				else:
					y1=y1+1
			if (j1 == l1-1) and d==8 :
				h = pv[num1[j][x1]][y1]
				if (h%2) == 0:
					h = h+1
				else:
					h = h-1+1
					# pv[num1[j][x1]][y1] = lsb(pv[num1[j][x1]][y1],c)
				pv[num1[j][x1]][y1] = h
				if y1==2:
					y1=0
					x1=x1+1
				else:
					y1=y1+1
			else:
				h = pv[num1[j][x1]][y1]
				if (h%2) == 0:
					h = h+0
				else:
					h = h-1+0
				
				# pv[num1[j][x1]][y1] = lsb(pv[num1[j][x1]][y1],c)
				pv[num1[j][x1]][y1] = h
				if y1==2:
					y1=0
					x1=x1+1
				else:
					y1=y1+1

def lsb2(a):
	if a%2 == 0:
		return 0
	else:
		return 1
def extr_dat(num1,k):
	ss =""
	for j in range(k):
		x1 = 0
		y1=0
		xy =0
		# gg =1
		while True :
			xy=0
			for i in range(8):
				df = lsb2(pv[num1[j][x1]][y1])
				xy = (xy*2)+df
				if y1==2:
					y1=0
					x1=x1+1
				else:
					y1=y1+1
			ss = ss+chr(xy)
			df = lsb2(pv[num1[j][x1]][y1])
			if df == 1:
				break
			else:
				if y1==2:
					y1=0
					x1=x1+1
				else:
					y1=y1+1

	return ss


def destag(adr2,k,iterations,centr):
	print("entered destag")
	num2,pv2,cent2,w1,h1 = clustr2(adr2,iterations,k,centr)
	xx = extr_dat(num2,k)
	return xx



def clustr2(img_adr,iterations,k,centr):

	im = Image.open(img_adr, "r")
	width, height = im.size
	l1 = [x for x in range(width*height)]
	# l2 = [x for x in range(height)]
	pix_val = list(im.getdata())
	# print(pix_val)
	pv = np.array(pix_val).reshape((width*height, 4))
	cent1 = sample(l1,k)
	num1 = []
	cent  = centr
	for x in range(iterations):
		num = [[]for x in cent1]
		cent11 = [[] for x in cent1]
		for y in range(width*height):
			mini = 99999
			up = 0
			for i in range(k):
				l = mini
				mini = min(mini,euc_dis(pv[y],cent[i]))
				if mini!=l:
					up = i
			if y not in num[up]:
				cent11[up].append(pv[y])
				num[up].append(y)
		for i in range(k):
			cent[i] = [float(sum(col))/len(col) for col in zip(*cent11[i])]
			# print(y)
		# print(x)
		num1 = num
	return num1,pv,cent,width,height


def clustr(img_adr,iterations,k):

	im = Image.open(img_adr, "r")
	width, height = im.size
	l1 = [x for x in range(width*height)]
	# l2 = [x for x in range(height)]
	pix_val = list(im.getdata())
	# print(pix_val)
	pv = np.array(pix_val).reshape((width*height, 4))
	cent1 = sample(l1,k)
	num1 = []
	cent  = [pv[x] for x in cent1]
	for x in range(iterations):
		if x==0:
			num = [[x] for x in cent1]
			cent11 = [[pv[x]] for x in cent1]
		else:
			num = [[]for x in cent1]
			cent11 = [[] for x in cent1]
		for y in range(width*height):
			mini = 99999
			up = 0
			for i in range(k):
				l = mini
				mini = min(mini,euc_dis(pv[y],cent[i]))
				if mini!=l:
					up = i
			if y not in num[up]:
				cent11[up].append(pv[y])
				num[up].append(y)
		for i in range(k):
			cent[i] = [float(sum(col))/len(col) for col in zip(*cent11[i])]
			# print(y)
		# print(x)
		num1 = num
	return num1,pv,cent,width,height

#########################################################################################
################    MAIN    #############################################################

img_adr2 = "newcrc.png"  				#image to be decrypted(has hidden data)
enc_txt = "hasjdncjanv=asdssd"			#string from AES
k = 5
img_adr = "crc.png"						#image to be encrypted
iterations = 10
num1,pv,cent,width,height = clustr(img_adr,iterations,k)

# print("wsea")
# print(cent)
ll = len(enc_txt)
n = int(ll/k)
parts = [enc_txt[i:i+n] for i in range(0,len(enc_txt),n)]
if len(parts) != k:
	parts[k-1] = parts[k-1]+(parts[k])
	del(parts[k])
# print(parts)


steg(parts,num1,k,pv)
newimg = Image.new('RGBA',(width,height))
pxx = totuple(pv)
newimg.putdata(pxx)
# newimg = Image.fromarray(pv)
newimg.save("newcrc.png")
# print("waiting")
# time.sleep(5)
# extr_dat(num1,k)
encryp_str = destag(img_adr2,k,iterations,cent)
print(encryp_str)











